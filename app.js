const express = require('express');
const fetch = require('cross-fetch');
const bodyParser = require('body-parser');

const PORT = 8888;

const app = express();

let token = '';
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/e-form-purchase', eFormPurchase);
app.post('/cabling-order', cablingOrder);
app.post('/equipment-relocation', equipmentRelocation);
app.post('/manager-contact', managerContactUpdate);
app.delete('/manager-contact', managerContactDelete);
app.post('/helpdesk-support', helpdeskSupport);
app.post('/software-installation', softwareInstall);
app.post('/hardware-installation', hardwareInstall);
app.post('/ticket-query-others', ticketQueryOthers);
app.post('/ticket-query-request', ticketQueryRequest);
app.post('/ticket-query-purchase', ticketQueryPurchase);

fetchToken();
setInterval(async () => {
    try {
        await fetchToken();
    } catch (e) {
        console.log('e', e);
    }
}, (60000 * 25));


async function fetchToken() {
    const res = await fetch('http://192.168.204.20:8008/api/jwt/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'username=chatbotadmin&password=P@ssw0rd',
    });

    token = await res.text();

    console.log('TOKEN: ', token);
}

async function eFormPurchase(req, res) {
    const {
        currentUserId,
        output,
        array,
        contactNumber,
        jobTitle,
        poApproval,
        divisionCode,
        costCenterCode,
        storageLocation,
    } = req.body;

    console.log('output', output);
    console.log('output', array);
    const parsedArray = JSON.parse(array);

    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface_Create?fields=values(Request Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'z1D Action': 'CREATE',
                    'Source Keyword': 'MyIT',
                    TitleInstanceID: 'SRGAA5V0GTI35AQC4J6JQB69V6JHZI',
                    'Login ID': currentUserId,
                    OfferingTitle: 'REST API TEST SRM Request 1',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                    Status: 'Submitted',
                    'SR Type Field 30': contactNumber,
                    'SR Type Field 31': jobTitle,
                    'SR Type Field 32': poApproval,
                    'SR Type Field 33': costCenterCode,
                    'SR Type Field 35': storageLocation,
                    'SR Type Field 34': divisionCode,
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', response);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        const requestNumber = responseJSON.values['Request Number'];
        for (let i = 0; i < parsedArray.length; i++) {
            const response = await fetch(
                `http://192.168.204.20:8008/api/arsys/v1/entry/ASL:SRM:RD:PurchaseRequestForm`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: token,
                    },
                    body: JSON.stringify({
                        values: {
                            Status: 'Enable',
                            Info_SRM_RequestNumber: requestNumber,
                            Info_ItemGenDesc: parsedArray[i].item,
                            Info_Price: parsedArray[i].price,
                            Info_Quantity: parsedArray[i].quantity,
                            Info_Subtotal: parsedArray[i].price * parsedArray[i].quantity,
                            Info_ItemCategory:
                                parsedArray[i].category[0].toUpperCase() + parsedArray[i].category.slice(1),
                        },
                    }),
                },
            );
        }
        res.status(200).json({ requestNumber });
    }
}

async function cablingOrder(req, res) {
    const {
        currentUserId,
        cablingLocation,
        cablingLanNode,
        cablingSwitchPort,
        cablingFirstContactName,
        cablingFirstContactNumber,
        cablingSecondContactName,
        cablingSecondContactNumber,
        cablingDate,
        cablingId,
    } = req.body;

    const descriptions = `Location: ${cablingLocation}
    Lan Node: ${cablingLanNode}
    Switch Port: ${cablingSwitchPort}
    First Contact Name: ${cablingFirstContactName}
    First Contact Number: ${cablingFirstContactNumber}
    Second Contact Name: ${cablingSecondContactName}
    Second Contact Number: ${cablingSecondContactNumber}
    Date: ${cablingDate}
    ID: ${cablingId}`;
    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface_Create?fields=values(Request Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'z1D Action': 'CREATE',
                    'Source Keyword': 'MyIT',
                    TitleInstanceID: 'SRGAA5V0GTI35AQC5XMEQB8IB10AY4',
                    'Login ID': currentUserId,
                    OfferingTitle: 'REST API TEST SRM Request 1',
                    Details: descriptions,
                    Status: 'Submitted',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', response);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ requestNumber: responseJSON.values['Request Number'] });
    }
}

async function equipmentRelocation(req, res) {
    const {
        currentUserId,
        relocationLocationCollect,
        relocationLocationRelocate,
        relocationContactCollect,
        relocationContactCollectNumber,
        relocationContactRelocate,
        relocationContactRelocateNumber,
        relocationMachineFrom,
        relocationMachineTo,
        relocationDate,
        relocationId,
    } = req.body;

    const descriptions = `Location to collect: ${relocationLocationCollect}
                            Location to relocate: ${relocationLocationRelocate}
                            Collection contact: ${relocationContactCollect}
                            Contact number (collect): ${relocationContactCollectNumber}
                            Relocate contact: ${relocationContactRelocate}
                            Contact number (relocate): ${relocationContactRelocateNumber}
                            Machine number from: ${relocationMachineFrom}
                            Machine number to: ${relocationMachineTo}
                            Date: ${relocationDate}
                            ID: ${relocationId}`;

    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface_Create?fields=values(Request Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'z1D Action': 'CREATE',
                    'Source Keyword': 'MyIT',
                    TitleInstanceID: 'SRGAA5V0GTI35AQHCMZFQGERJQJ5DG',
                    'Login ID': currentUserId,
                    OfferingTitle: 'REST API TEST SRM Request 1',
                    Details: descriptions,
                    Status: 'Submitted',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', response);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ requestNumber: responseJSON.values['Request Number'] });
    }
}

async function managerContactUpdate(req, res) {
    const {
        currentUserId,
        contactCMStorage,
        contactCMId,
        contactCMNumber,
        contactCMDivision,
        contactCMCenter,
        contactCMSupervision,
        contactCMInCharge,
    } = req.body;

    const descriptions = `
   Type: "Update"
   Storage location: ${contactCMStorage}
   New PC Manager ID: ${contactCMId}
   New PC Manager number: ${contactCMNumber}
   New PC manager division: ${contactCMDivision}
   Cost center: ${contactCMCenter}
   Supervision of PC Manager: ${contactCMSupervision}
   Office in Charge: ${contactCMInCharge}`;

    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface_Create?fields=values(Request Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'z1D Action': 'CREATE',
                    'Source Keyword': 'MyIT',
                    TitleInstanceID: 'SRGAA5V0GTI35AQHCLROQGEQCJJ45V',
                    'Login ID': currentUserId,
                    OfferingTitle: 'REST API TEST SRM Request 1',
                    Details: descriptions,
                    Status: 'Submitted',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', response);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ requestNumber: responseJSON.values['Request Number'] });
    }
}

async function managerContactDelete(req, res) {
    const {
        currentUserId,
        contactDStorage,
        contactDId,
        contactDNumber,
        contactDDivision,
        contactDCenter,
        contactDSupervision,
        contactDInCharge,
    } = req.body;

    const descriptions = `
    Type: "Delete"
    Storage location: ${contactDStorage}
    PC Manager ID: ${contactDId}
    PC Manager number: ${contactDNumber}
    PC manager division: ${contactDDivision}
    Cost center: ${contactDCenter}
    Supervision of PC Manager: ${contactDSupervision}
    Office in Charge: ${contactDInCharge}`;

    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface_Create?fields=values(Request Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'z1D Action': 'CREATE',
                    'Source Keyword': 'MyIT',
                    TitleInstanceID: 'SRGAA5V0GTI35AQHCLROQGEQCJJ45V',
                    'Login ID': currentUserId,
                    OfferingTitle: 'REST API TEST SRM Request 1',
                    Details: descriptions,
                    Status: 'Submitted',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', response);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ requestNumber: responseJSON.values['Request Number'] });
    }
}


async function helpdeskSupport(req, res) {
    const { currentUserId, machine, machine_name, serialNumber, warranty, fault_symptoms, machine_flow_name, machine_flow_phone} = req.body;

    let descriptions = ""
    if (warranty){
        descriptions = `${machine} name: ${machine_name}, serial: ${serialNumber} status: Under warranty`;
    }else{
        descriptions = `${machine} name: ${machine_name}, serial: ${serialNumber} status: Not under warranty issues: ${fault_symptoms} name: ${machine_flow_name} contact: ${machine_flow_phone}`;
    }
    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/HPD:IncidentInterface_Create?fields=values(Incident Number)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    Login_ID: currentUserId,
                    Description: descriptions,
                    Impact: '1-Extensive/Widespread',
                    Urgency: '1-Critical',
                    Status: 'Assigned',
                    'Reported Source': 'Direct Input',
                    Service_Type: 'User Service Restoration',
                    Company: 'Housing Authority',
                    z1D_Action: 'CREATE',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', responseJSON);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ incidentNumber: responseJSON.values['Incident Number'] });
    }
}

async function softwareInstall(req, res) {
    const {
        firstName,
        lastName,
        softwareFlow_software,
        softwareFlow_ehousingid,
        softwareFlow_number,
        softwareFlow_address,
        softwareFlow_machineNumber,
    } = req.body;

    const descriptions = `Software: ${softwareFlow_software} e-housing id: ${softwareFlow_ehousingid}, number: ${softwareFlow_number} address: ${softwareFlow_address} machine number: ${softwareFlow_machineNumber}`;
    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/WOI:WorkOrderInterface_Create/?fields=values(WorkOrder_ID)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    'Detailed Description': descriptions,
                    z1D_Action: 'CREATE',
                    'First Name': firstName,
                    'Last Name': lastName,
                    'Customer First Name': firstName,
                    'Customer Last Name': lastName,
                    Summary: 'Software installation',
                    Status: 'Assigned',
                    Company: 'Housing Authority',
                    'Customer Company': 'Housing Authority',
                    'Location Company': 'Housing Authority',
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', responseJSON);
    if (response.status !== 201) {
        res.status(response.status).json({ msg: responseJSON[0].messageText });
    } else {
        res.status(response.status).json({ workorderId: responseJSON.values['WorkOrder_ID'] });
    }
}

async function hardwareInstall(req, res) {
    const {
        firstName,
        lastName,
        hardwareType,
        hardware_ehousingid,
        hardware_number,
        hardware_address,
        hardware_machineNumber,
    } = req.body;

    const descriptions = `Hardware: ${hardwareType} e-housing id: ${hardware_ehousingid}, number: ${hardware_number} address: ${hardware_address} machine number: ${hardware_machineNumber}`;
    const response = await fetch(
        `http://192.168.204.20:8008/api/arsys/v1/entry/WOI:WorkOrderInterface_Create/?fields=values(WorkOrder_ID)`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: token,
            },
            body: JSON.stringify({
                values: {
                    "Detailed Description": descriptions,
                    "z1D_Action": "CREATE",
                    "First Name": firstName,
                    "Last Name": lastName,
                    "Customer First Name": firstName,
                    "Customer Last Name": lastName,
                    "Summary": "Hardware installation",
                    "Status": "Assigned",
                    "Company": "Housing Authority",
                    "Customer Company": "Housing Authority",
                    "Location Company": "Housing Authority",
                },
            }),
        },
    );

    const responseJSON = await response.json();

    console.log('res', responseJSON);
    if (response.status !== 201) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        res.status(response.status).json({ workorderId: responseJSON.values['WorkOrder_ID'] });
    }
}

async function ticketQueryOthers(req, res) {
    const {
        ticketType,
        formID
    } = req.body;

    const types = {
        work_order: {
            interface: "WOI:WorkOrderInterface",
            id: "Work Order ID" 
        },
        incident: {
            interface: "HPD:IncidentInterface",
            id: "Incident Number"
        }
    }

    const response = await fetch(`http://192.168.204.20:8008/api/arsys/v1/entry/${types[ticketType].interface}/?q='${types[ticketType].id}'="${formID}"`,{
        headers: {
            Authorization: token
        },
    })

    const responseJSON = await response.json();

    console.log('res %j', responseJSON);
    if (response.status !== 200) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        const {entries} = responseJSON;
        console.log('entries', entries.length);
    if (entries.length === 0){
            res.status(response.status).json({ found: false, msg: "Ticket not found." });
        }else{
            const submitter = entries[0].values["Submitter"];
            const submitDate = new Date(entries[0].values["Submit Date"]).toLocaleDateString();
            const status = entries[0].values["Status"];
            const description = entries[0].values["Description"]|| "There are no descriptions to this ticket.";
            res.status(response.status).json({ submitter,submitDate, status, description, found: true});
        }
    }
}

async function ticketQueryRequest(req, res) {
    const {
        formID
    } = req.body;

    const response = await fetch(`http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface/?q='Request Number'="${formID}"`,{
        headers: {
            Authorization: token
        },
    })

    const responseJSON = await response.json();

    console.log('res %j', responseJSON);
    if (response.status !== 200) {
        console.log('error', responseJSON)
        res.status(response.status).json({ msg: responseJSON });
    } else {
        const {entries} = responseJSON;
        console.log('entries', entries.length);

        if (entries.length === 0){
            res.status(response.status).json({found: false, msg: "Ticket not found." });
        }else{
            const submitter = entries[0].values["Submitter"] 
            const submitDate = new Date(entries[0].values["Submit Date"]).toLocaleDateString();
            const status = entries[0].values["Status"] 
            const details = entries[0].values["Details"] || "There are no details to this ticket.";
            res.status(response.status).json({ submitter,submitDate, status, details, found: true });
        }
        
    }
}

async function ticketQueryPurchase(req, res) {
    const {
        formID,
        userId
    } = req.body;

    const requestResponse = await fetch(`http://192.168.204.20:8008/api/arsys/v1/entry/SRM:RequestInterface/?q='Request Number'="${formID}"`,{
        headers: {
            Authorization: token
        },
    })
    const formResponse = await fetch(`http://192.168.204.20:8008/api/arsys/v1/entry/ASL:SRM:RD:PurchaseRequestForm?q='Info_SRM_RequestNumber'="${formID}"`,{
        headers: {
            Authorization: token
        },
    })
    await fetch(`http://common-functions.default.svc.cluster.local:8080/common-functions/endSession`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "projectId":"HOUSING",
            "userId":userId
        })
    })

    const responseJSON = await requestResponse.json();
    const formResponseJSON = await formResponse.json();
    console.log("formResponseJSON",formResponseJSON)
    let output = ""
    if(formResponseJSON.entries.length === 0){
        output = "There are no items under this Service Request."
    }
    else{
        for (let entry of formResponseJSON.entries){
            const purchased = {
                item: entry.values['Info_ItemGenDesc'],
                price: entry.values['Info_Price'],
                quantity: entry.values['Info_Quantity'],
                category: entry.values['Info_ItemCategory'],
                description: entry.values['Info_ItemGenDesc'],
            }
            const res = await fetch(`http://common-functions.default.svc.cluster.local:8080/common-functions/pushToArray`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "projectId":"HOUSING",
                    "userId":userId,
                    "type":"price",
                    "variable": "purchased",
                    "item": purchased
                })
            })
            const outputResponse = await res.json()
            console.log('outputResponse', outputResponse);
    
            output = outputResponse.output.output
        }

    }

    console.log('res %j', responseJSON);
    console.log('output %j', output);
    if (requestResponse.status !== 200) {
        console.log('error', responseJSON)
        res.status(requestResponse.status).json({ msg: responseJSON });
    } else {
        const {entries} = responseJSON;
        console.log('entries', entries.length);
        if (entries.length === 0){
            res.status(requestResponse.status).json({found: false, msg: "Ticket not found." });
        }else{
            const submitter = entries[0].values["Submitter"];
            const submitDate = new Date(entries[0].values["Submit Date"]).toLocaleDateString();
            const status = entries[0].values["Status"];
            res.status(requestResponse.status).json({ submitter,submitDate, status, output, found: true});
        }
        
    }
}


app.listen(PORT, () => console.log(`Listening to localhost:${PORT}`));
